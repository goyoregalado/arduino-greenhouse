# MH-RD Raindrop Sensor
This sensor measures the presence of raindrops on a surface.

## Wiring
This is an analog sensor, but it also has an digital output. In this case we are going to use
the analog interface because we can extract more information from it.
The way this sensor operates is just using the raindrops as a way of modifying the resistance of the
circuit.
Taking into account this Ohm's law V = I*R so as resistance grows so does the voltaje.
We will connect this sensor to A2 pin.
 
## Datasheet
There isn't such a datasheet but we have found this kind of documents. [Datasheet link](./rain_sensor_module.pdf)

## Library
This is just an analogic sensor, so we don't need any library, we read it values directly from an Analog Pin.