# UVM-30A UV detection sensor
This sensor is built around a [guva-s12sd sensor module](./guva-s12sd.pdf)
This device allows us to measure the sunlight UV intensity and this parameter can be easily related to UV Index.    
It detects UV radiation in the wavelength range between 200nm-370nm.

## Wiring
This sensor has an analog interface so we must wire it to an arduino analog port. We will connect this sensor to 
A1 pin.
 
## Datasheet
It's difficult to find the correct datasheet for this module so we have looked around for a couple of
modules that integrate the same sensing element, all of them refers the same output data so here is the list.

 [Generic UV module](./ultraviolet.pdf)
 [Seed UV sensor module](./ultraviolet_seed.pdf)

 From this couple of datasheets we extract next correspondences:

![UV Sensor analog output](./img/UV_Sensor_mvOutput.jpg)

![UV Sensor output and UV Index relationship](./img/UV_Sensorindex.jpg)

## Library
There's no need to use any library to read this sensor output, but we must take into account that it uses a 12 bits ADC conversion to stablish the relationship between output and UV Index, so we must translate it to a 10 bits ADC conversion wich is the ADC resolution of Arduino.


## References

* https://polaridad.es/sensor-radiacion-ultravioleta-arduino-indice-uv-uvm30a-guva-s12sd/
