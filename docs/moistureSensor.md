# Capacitive soil moisture sensor V2.0
This sensor measures the moisture of the soil.
This will need a calibration process to determine the sensitivity of the device.

## Wiring
This is an analog sensor, the way this sensor operates is just using the moisture as a way of modifying the resistance of the
circuit.
Taking into account this Ohm's law V = I*R so as resistance grows so does the voltaje.
We will connect this sensor to A3 pin.
 
## Datasheet
There isn't such a datasheet but we have found this kind of documents. [Datasheet link](./moisture_sensor.pdf)

## Library
This is just an analogic sensor, so we don't need any library, we read it values directly from an Analog Pin.