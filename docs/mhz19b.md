# MH-Z19B CO2 Sensor
This sensor is intended to measure the concentration of CO2 gas.

## Wiring
In this case, we have two different interfaces a PWM interface and a serial interface, we are going to use a serial port.
To leave the hardware UART free to use as an interface with the greenhouse server (the Raspi) we will use SoftwareSerial library that comes bundled with Arduino and we will set a software serial port using pin 10 (RX) and pin 11 (TX)
 
## Datasheet
The datasheet is linked [here](./MH-Z19B.pdf)

## Library
To use this sensor properly we are using the [library by strange-v](https://github.com/strange-v/MHZ19)
We also host a local copy of this library that is ready to install with the Arduino IDE, you can 
reach it in this link [MHZ19](./MHZ19.zip)