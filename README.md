# Arduino controlled Greenhouse project.

## Main objective.

The main purpouse of this project it's to have an Arduino based interface to control the sensors installed into a DIY greenhouse and to manage the electro-valves needed to irrigate.

## System elements.

### Sensors.

* [MG811 CO2 gas sensor.](./docs/mg811.md)
* [MH-Z19B Infrared CO2 sensor.](./docs/mhz19b.md)
* NH-Z14A CO2 Gas Sensor.
* [MH-RD Raindrop sensor.](./docs/mhrd.md)
* [Capacitive soil moisture sensor v2.0.](./docs/moistureSensor.md)
* [DS18B20 Digital temperature sensor.](./docs/ds18b20.md)
* [UVM30A UV Detection Sensor.](./docs/uvm30a.md)
* [BH1750 Ambient light sensor.](./docs/bh1750.md)


### Actuators. 
* 12 V. Electro-valve 3/4"

## Communications.
This system is intended to communicate with a Raspberry 4 module via a Serial connection.
The system should be able to attend the following commands.

1. Status.
Returns a really verbose output with the status of all sensors involved.

2. CO2 sensor read.
Returns the CO2 ppm readed by CO2 sensor.

3. Raindrop sensor read.
Returns a value between 0 and 1023 representing the analog read of this sensor.
Highest values represents less raindrops.

4. Moisture sensor read.
Returns a value between 0 and 1023 representing the analog read of this sensor.
Highest values represents less moisture.

5. Temperature sensor read.
Returns a float with the temperature measured in Celsius degrees.

6. UV sensor read.
Returns a value between 0 and 1023 that has a correspondence with UV Index by OMS.
Take a look to the [sensor documentation page](./docs/uvm30a.md) to translate it correctly.

7. Ambient light sensor read.
Returns a integer that represent the ambient light measured in lux.

8. Turn off electro-valve.
Not implemented yet.

9. Turn on electro-valve.
Not implemented yet.

If you send any other value then you'll obtain this exact message: "Error: Invalid option"

Every message ends up with a new line character "\n"

