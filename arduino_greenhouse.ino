#include <Wire.h>
#include <BH1750.h>

#include <OneWire.h>
#include <DallasTemperature.h>

#include <SoftwareSerial.h>

#include <MHZ19.h>
#include <MHZ19PWM.h>


// Number of samples used to average analog reads.
const int numSamples = 10;

char incomingMessage[64];

//Arguments are RXpin, TXpin
SoftwareSerial co2Serial(10, 11);

MHZ19 mhz(&co2Serial);

const int uvSensorPin = A1;

const int tempSensorPin = 8;
OneWire oneWire(tempSensorPin);
DallasTemperature tempSensor(&oneWire);

BH1750 lightSensor;

const int rainDropSensorPin = A2;

const int moistureSensorPin = A3; 


int readCO2Sensor() {
  MHZ19_RESULT response = mhz.retrieveData();
  if (response == MHZ19_RESULT_OK) {
    return mhz.getCO2();
  }
  else {
    return -1;
  }
}

int readUVSensor() {
  return readAnalogSensor(uvSensorPin);
};

float readTemperatureSensor(int sensorId){
  tempSensor.requestTemperatures();
  return tempSensor.getTempCByIndex(sensorId);
}

int readLightSensor() {
  return lightSensor.readLightLevel();
}

int readRainDropSensor() {
  return readAnalogSensor(rainDropSensorPin);
}

int readMoistureSensor() {
  return readAnalogSensor(moistureSensorPin);
}

int readAnalogSensor(int pin) {
  int value = 0;
  for (int i = 0; i < numSamples; i++) {
    value += analogRead(pin); 
  }
  value = value/numSamples;
  return value;
}

void readGlobalStatus() {
  MHZ19_RESULT response = mhz.retrieveData();
  Serial.println(F("==== CO2 Sensor ===="));
  if (response == MHZ19_RESULT_OK) {
    Serial.print(F("    => CO2: "));
    Serial.println(mhz.getCO2());
    Serial.print(F("    => Accuracy: "));
    Serial.println(mhz.getAccuracy());
  }
  else {
    Serial.print(F("Error, code: "));
    Serial.println(response);
  }
  Serial.println(F("====ooO0Ooo===="));

  int uvValue = readUVSensor();
  Serial.println(F("==== UV Sensor ===="));
  Serial.print(F("    => UV value: "));
  Serial.println(uvValue);
  Serial.println(F("====ooO0Ooo===="));

  float tempValue = readTemperatureSensor(0);
  Serial.println(F("==== Temperature ==== "));
  Serial.print(tempValue);
  Serial.println("ºC");
  Serial.println(F("====ooO0Ooo===="));

  int lightIntensity = readLightSensor();
  Serial.println(F("==== Light intensity ==== "));
  Serial.print(lightIntensity);
  Serial.println("lux");
  Serial.println(F("====ooO0Ooo===="));

  int rainDropValue = readRainDropSensor();
  Serial.println(F("==== Rain drop ==== "));
  Serial.print(rainDropValue);
  Serial.println(" no units ");
  Serial.println(F("====ooO0Ooo===="));

  int moistureValue = readMoistureSensor();
  Serial.println(F("==== Moisture ==== "));
  Serial.print(moistureValue);
  Serial.println(" no units ");
  Serial.println(F("====ooO0Ooo===="));
}

void setup() {
  Serial.begin(9600);
  Serial.println(F("Initialization GreenHouse Control"));
  
  co2Serial.begin(9600);
  Wire.begin();
  lightSensor.begin(BH1750::CONTINUOUS_HIGH_RES_MODE);
  Serial.println(F("Completed ... Status [OK]"));
}

void loop() {
  int bytesReaded = 0;
  if (Serial.available() > 0) {
    bytesReaded = Serial.readBytesUntil('\n', incomingMessage, 64);
    String str = String(incomingMessage);
    int value = str.toInt();

    switch(value) {
      case 1:
        readGlobalStatus();
        break;
      case 2:
        Serial.println(readCO2Sensor());
        break;
      case 3:
        Serial.println(readRainDropSensor());
        break;
      case 4:
        Serial.println(readMoistureSensor());
        break;
      case 5:
        Serial.println(readTemperatureSensor(0));
        break;
      case 6:
        Serial.println(readUVSensor());
        break;
      case 7:
        Serial.println(readLightSensor());
        break;
      default:
        Serial.println(F("Error: Invalid option"));
        break;         
    }
  }
}
